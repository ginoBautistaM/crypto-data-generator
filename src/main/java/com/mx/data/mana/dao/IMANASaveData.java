package com.mx.data.mana.dao;

import java.util.List;

import com.mx.data.mana.entity.MANAEntity;
import com.mx.data.mana.entity.MANAHistoricalOrder;

public interface IMANASaveData {
	
	public void startConection();
	
	void insertCurrentCryptoPrice(MANAEntity mana);
	
	void insertHistoricalBidAndOfferRegister(List<MANAHistoricalOrder> manaOrder);
	
	int getLastQueryId();

}
