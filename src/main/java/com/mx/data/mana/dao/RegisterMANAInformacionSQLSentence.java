package com.mx.data.mana.dao;

public interface RegisterMANAInformacionSQLSentence {
	
	public static final String INSERT_CURRENTPRICE = "INSERT INTO trading.mana_historical_fluctuation(last_price, created_time, vwap) values(?, ?, ?)".intern();
	
	public static final String INSERT_BIDANDOFFER = "INSERT INTO trading.mana_historical_order(id_query, price, amount, type) values(?, ?, ?, ?)".intern();
	
	public static final String GET_LASTQUERYID = "select case when max(id_query) is null then 0 else max(id_query) end from trading.mana_historical_fluctuation".intern();

}
