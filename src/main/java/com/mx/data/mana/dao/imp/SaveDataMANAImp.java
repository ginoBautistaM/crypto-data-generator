package com.mx.data.mana.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.mx.data.mana.dao.IMANASaveData;
import com.mx.data.mana.dao.RegisterMANAInformacionSQLSentence;
import com.mx.data.mana.entity.MANAEntity;
import com.mx.data.mana.entity.MANAHistoricalOrder;

public class SaveDataMANAImp extends JdbcDaoSupport implements IMANASaveData {

	private Connection conn;
	
	@Override
	public void startConection() {
		this.conn = DataSourceUtils.getConnection(getDataSource());
		setDataSource(new SingleConnectionDataSource(this.conn, true));
		this.createJdbcTemplate(getDataSource());
		
	}

	@Override
	public void insertCurrentCryptoPrice(MANAEntity mana) {
		getJdbcTemplate().update(RegisterMANAInformacionSQLSentence.INSERT_CURRENTPRICE
				, mana.getLast_price()
				, mana.getCreated_time().substring(0,10) + " " +  mana.getCreated_time().substring(11,19)
				, mana.getVwap());
		insertHistoricalBidAndOfferRegister(mana.getHistoricalRegister());
		
	}

	@Override
	public void insertHistoricalBidAndOfferRegister(List<MANAHistoricalOrder> manaOrder) {
		int queryId = getLastQueryId();
		this.getJdbcTemplate().batchUpdate(RegisterMANAInformacionSQLSentence.INSERT_BIDANDOFFER,
				new BatchPreparedStatementSetter() {
			
					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setInt(1, queryId);
						ps.setString(2, manaOrder.get(i).getPrice());
						ps.setString(3, manaOrder.get(i).getAmount());
						ps.setString(4, manaOrder.get(i).getType());
					}

					@Override
					public int getBatchSize() {
						return manaOrder.size();
					}

				});
	}

	@Override
	public int getLastQueryId() {
		return Integer.valueOf(getJdbcTemplate().queryForObject(RegisterMANAInformacionSQLSentence.GET_LASTQUERYID
				, String.class));
	}

}
