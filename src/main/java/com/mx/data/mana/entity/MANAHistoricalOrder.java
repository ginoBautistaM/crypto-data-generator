package com.mx.data.mana.entity;

public final class MANAHistoricalOrder {

	private String price;
	private String amount;
	private String type;
	
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "MANAHistoricalOrder [price=" + price + ", amount=" + amount + ", type=" + type + "]";
	}
	
	
}
