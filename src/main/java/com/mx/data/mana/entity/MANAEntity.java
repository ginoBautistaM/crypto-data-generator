package com.mx.data.mana.entity;

import java.util.List;

public class MANAEntity {
	
	private String last_price;
	private String created_time;
	private String vwap;
	public List<MANAHistoricalOrder> historicalRegister;
	
	public String getLast_price() {
		return last_price;
	}
	public void setLast_price(String last_price) {
		this.last_price = last_price;
	}
	public String getCreated_time() {
		return created_time;
	}
	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}
	public String getVwap() {
		return vwap;
	}
	public void setVwap(String vwap) {
		this.vwap = vwap;
	}
	public List<MANAHistoricalOrder> getHistoricalRegister() {
		return historicalRegister;
	}
	public void setHistoricalRegister(List<MANAHistoricalOrder> historicalRegister) {
		this.historicalRegister = historicalRegister;
	}
	@Override
	public String toString() {
		return "MANAEntity [last_price=" + last_price + ", created_time=" + created_time + ", vwap=" + vwap;
	}
	
	
	
	
}
