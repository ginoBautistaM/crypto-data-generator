package com.mx.data.mana.main;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mx.data.mana.bitso.service.BaseBitsoCryptoSearching;
import com.mx.data.mana.bitso.service.ISearchCryptoInformation;
import com.mx.data.mana.bitso.service.imp.SearchBitsoManaInformationImpl;
import com.mx.data.mana.dao.IMANASaveData;
import com.mx.data.mana.dao.imp.SaveDataMANAImp;
import com.mx.data.mana.entity.MANAEntity;
import com.mx.data.mana.entity.MANAHistoricalOrder;

public class MainProcess {
	
	public static AnnotationConfigApplicationContext context;
	
	
	public static final Logger logger  = Logger.getLogger(MainProcess.class);
	
	public static void main(String args[]) throws IOException, InterruptedException {
		int counterCycle = 1;
		boolean isCorrectContextConfiguration = false;
		long temporalStart = 0;
		StringBuilder currentPrice = new StringBuilder();
		StringBuilder bidsAndOffer = new StringBuilder();
		SaveDataMANAImp dataMana = null;
		MANAEntity mana = new MANAEntity();
		MANAHistoricalOrder historicalMana = new MANAHistoricalOrder();
		JSONObject obj = null;
		JSONArray arrayJson = null;
		Instant initialInstant = null;
		Instant endedInstant = null;
		ISearchCryptoInformation bitsoService = null;
		logger.info("Inicializando application context.");
		isCorrectContextConfiguration = startContext();
		if(!isCorrectContextConfiguration) {
			logger.fatal("Error al levantar contexto de aplicación.");
			System.exit(3);
		}
		logger.info("Contexto levantado exitosamente.");
		dataMana = (SaveDataMANAImp) context.getBean(IMANASaveData.class);
		bitsoService = new SearchBitsoManaInformationImpl();
		while(counterCycle < BaseBitsoCryptoSearching.MAX_REQUESTPERDAY_PUBLICAPI) {
			// Consulta de precio 
			try {
				initialInstant = Instant.now();
				currentPrice.append(bitsoService.queryCurrentPrice());
				obj = new JSONObject(currentPrice.toString());
				mana.setLast_price(obj.getJSONObject("payload").getString("last"));
				mana.setVwap(obj.getJSONObject("payload").getString("vwap"));
				mana.setCreated_time(obj.getJSONObject("payload").getString("created_at"));
				mana.setHistoricalRegister(new ArrayList<MANAHistoricalOrder>());
				mana.setHistoricalRegister(new ArrayList<MANAHistoricalOrder>());
				// Consulta de pujas 
				bidsAndOffer.append(bitsoService.queryBidsAndOffer());
				obj = new JSONObject(bidsAndOffer.toString());
				arrayJson = obj.getJSONObject("payload").getJSONArray("bids"); 
				for (int i = 0; i < arrayJson.length(); i++){
					historicalMana = new MANAHistoricalOrder();
				    historicalMana.setPrice(arrayJson.getJSONObject(i).getString("price"));
				    historicalMana.setAmount(arrayJson.getJSONObject(i).getString("amount"));
				    historicalMana.setType(ISearchCryptoInformation.COMPRA);
				    mana.getHistoricalRegister().add(historicalMana);
				}
				arrayJson = obj.getJSONObject("payload").getJSONArray("asks"); 
				for (int i = 0; i < arrayJson.length(); i++){
					historicalMana = new MANAHistoricalOrder();
				    historicalMana.setPrice(arrayJson.getJSONObject(i).getString("price"));
				    historicalMana.setAmount(arrayJson.getJSONObject(i).getString("amount"));
				    historicalMana.setType(ISearchCryptoInformation.VENTA);
				    mana.getHistoricalRegister().add(historicalMana);
				}
				// Guardarlo en base de datos.
				
				dataMana.insertCurrentCryptoPrice(mana);
				logger.info(mana.toString());
				counterCycle ++;
			} catch(NullPointerException errorBitsoService) {
				currentPrice.setLength(0);
				bidsAndOffer.setLength(0);
			} finally {
				endedInstant = Instant.now();
				temporalStart = Duration.between(initialInstant, endedInstant).getNano();
				temporalStart = temporalStart < 1000 ? temporalStart : 1000 - temporalStart;
				Thread.sleep(temporalStart);
			}
		}
		
	}
	
	public static boolean startContext() {
		try {
			context = new AnnotationConfigApplicationContext();
            context.scan("com.mx.data.mana.main");
            context.refresh();
            logger.info("Contexto generado exitosamente...");
            return true;
		} catch(BeansException | IllegalStateException error) {
			logger.fatal("Error al generar contextos de Spring...", error);
			return false;
		}
	}

}
