package com.mx.data.mana.main;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.mx.data.mana.dao.IMANASaveData;
import com.mx.data.mana.dao.imp.SaveDataMANAImp;

@Configuration
@PropertySource("classpath:/manaDataGenerator.properties")
public class Context {
	
	@Value("${urlDataBase}")
	private String urlDataBase;
	
	@Value("${usserDataBase}")
	private String usserDataBase;
	
	@Value("${autDataBase}")
	private String autDataBase;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean(name = "dataConnectionMySQL")
	public IMANASaveData dataConnectionMySQL() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		IMANASaveData saveData = new SaveDataMANAImp();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl(urlDataBase );
		dataSource.setUsername(usserDataBase);
		dataSource.setPassword(autDataBase);
		((SaveDataMANAImp) saveData).setDataSource(dataSource);
		return saveData;
	}

}
