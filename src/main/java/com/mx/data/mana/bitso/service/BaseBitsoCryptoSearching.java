package com.mx.data.mana.bitso.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

public abstract class BaseBitsoCryptoSearching {
	
	public final String SERVICE_CURRENTPRICE = "https://api.bitso.com/v3/ticker/?book=mana_mxn".intern();
	public final String SERVICE_OFFERSANDBIDS = "https://api.bitso.com/v3/order_book/?book=mana_mxn&aggregate=true".intern();
	public static final int MAX_REQUESTPERDAY_PUBLICAPI = 86400;
	private static final Logger logger = Logger.getLogger(BaseBitsoCryptoSearching.class);
	
	protected StringBuilder bitsoQueryPublicService(String service) throws IOException {
		int responseCode = -1;
		StringBuilder response = null;
		String inputLine =null;
        URL obj = new URL(service);
        HttpURLConnection con = null;
        BufferedReader in = null;
        try {
        	con = (HttpURLConnection) obj.openConnection();
        	con.setRequestProperty("User-Agent", "Bitso Java Example");
            con.setRequestMethod("GET");
            responseCode = con.getResponseCode();
	        in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	        response = new StringBuilder();
	        while ( (inputLine= in.readLine()) != null) {
	            response.append(inputLine);
	        }
	        in.close();
        } catch(IOException errorSearchingBitsoData) {
        	logger.error("Error al consultar servicio de bitso. Estatus devuelto: " + responseCode);
        	throw errorSearchingBitsoData;
        }
        return response;
	}

}
