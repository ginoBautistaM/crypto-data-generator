package com.mx.data.mana.bitso.service;

import java.io.IOException;

public interface ISearchCryptoInformation {
	
	static final  String COMPRA = "bid".intern();
	static final  String VENTA = "ask".intern();
	
	String queryCurrentPrice() throws IOException;
	
	String queryBidsAndOffer() throws IOException;

}
