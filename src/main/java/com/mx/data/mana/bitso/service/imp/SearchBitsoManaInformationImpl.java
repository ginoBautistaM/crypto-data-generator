package com.mx.data.mana.bitso.service.imp;

import java.io.IOException;

import com.mx.data.mana.bitso.service.BaseBitsoCryptoSearching;
import com.mx.data.mana.bitso.service.ISearchCryptoInformation;

public class SearchBitsoManaInformationImpl extends BaseBitsoCryptoSearching implements ISearchCryptoInformation {

	@Override
	public String queryCurrentPrice() throws IOException {
		return bitsoQueryPublicService(SERVICE_CURRENTPRICE).toString();
	}

	@Override
	public String queryBidsAndOffer() throws IOException {
		return bitsoQueryPublicService(SERVICE_OFFERSANDBIDS).toString();
	}

}
